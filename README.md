Climate Tracker with LoRaWAN - ChripStack Application Server
-------------------------------------------------------------

**Contact**: <revol-xut@protonmail.com>

This repository contains the configuration and setup files of the ChirpStack Application Server furthermore implementation of varius endpoints
which get called if the App Server is receiving messages from the microcontrollers.

## Used Software

List of software that can be installed by this script. 

- Dragino Package or Chirpstack Concentratord
- Chirpstack Gateway
- Mosquitto
- Chirpstack Network Server
- Redis
- Chirpstack Application Server

## Deployment

Before you start enable spi in your raspberry pi.

There will be multiple setup scripts that will install all the required software, generate the configuration and setup the influxdb.

**Installation**:

```bash
	./chirp_stack.sh
```


## Debug

Checking if the Gateways Sends data: sudo tcpdump -AUq -i lo port 1700


## Change Log

**v0.0a** (30.4.2020)

	- added distro-dectection
	- essential installation as descriped on website
	- start dialog boxes
    - grafana
    - chirpstack
