//ChirpStack Setup Script for Raspberry Pis
//by Tassilo Tanneberger <revol-xut@protonmail.com>
//License: GPLv2


function Decode(fPort, bytes, variables) {
  var return_value = {};

  if(fPort == 68){
    if(bytes.length >= 2){
    	return_value.battery = ((bytes[0] << 8) | bytes[1]) / 1000.0; // To V
    }
    if (bytes.length >= 4) {
      return_value.temperature = (((bytes[2] << 8) | bytes[3]) - 40.0) / 10.0;
    }
    if (bytes.length >= 6) {
      return_value.pressure = ((bytes[4] << 8) | bytes[5]);
    }
    if (bytes.length >= 8) {
      return_value.humidity = (((bytes[6] << 8) | bytes[7]) / 10.0);
    }
    if (bytes.length >= 10) {
      return_value.concentration = ((bytes[8] << 8) | bytes[9]);
    }
  }
  return return_value;
}
