#!/bin/sh
# ChirpStack Setup Script for Raspberry Pis and Odroids
# by Tassilo Tanneberger <revol-xut@protonmail.com>
# License: GPLv2

ping -q -w 1 -c 1 `ip r | grep default | cut -d ' ' -f 3` > /dev/null && CON="TRUE" || CON="FALSE"
