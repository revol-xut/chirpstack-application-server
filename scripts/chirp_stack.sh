#!/bin/sh
# ChirpStack Setup Script for Raspberry Pis
# by Tassilo Tanneberger <revol-xut@protonmail.com>
# License: GPLv2

GRAPHANA="False"

. ./detect_distro.sh
. ./check_connection.sh
. ./debian_installation.sh


is_dialog_installed(){
  eval 'which dialog' 
  
  if [ "$?" -eq 1 ]; then
    echo "dialog is not installed. processing to installing it ..."
    echo "$DISTRO"
    if [ "$DISTRO" = "debian" ]; then
      apt install dialog
    elif [ "$DISTRO" = "arch" ]; then
      pacman -S dialog
    else
      echo "could not find your package manager aborting"
      exit 1
    fi
  else
    echo "dialog found"
  fi

}


welcomedialog(){ \
	dialog --title "Welcome !" --msgbox "This is the server setup script for Climate Tacker with LoraWAN project\n\nThis script will automatically install all the required chirpstack software, setup a InfluxDB and optionally a graphana instance for visualisation purposes !" 10  50

	dialog --title "Important !" --yes-label "Start !" --no-label "Abort !" --yesno "Are you sure you want to install the chirpstack software\nThere is no turning back after this. \n\nDetected OS: $OS\nVersion: $VERSION\nBase distro: $DISTRO\nConnected: $CON" 11 70 
}


abort_duo_connection(){
	. ./check_connection.sh
	if [ "$CON" = "FALSE" ]; then
		dialog --infobox "Abort because you are not connected to the internet !\nCONNECTION: $CON\n\nEstablish a connection an rerun this script" 4 70
		exit 1
	else
		echo "found internet connection"
	fi

}

input_postgres_settings(){
	dialog --title "Setting up the Postgres Database !" --yes-label "Default" --no-label "Manual" --yesno "Do you want to use the fault settings for your postgres database ?" 10 50
	response=$?

	if [ "$response" -eq 0 ]; then
		echo "Default values for installation ..."	
		username_network="chirpstack_ns"
		password_network="network_pw"

		username_app="chirpstack_as"
		password_app="application_pw"

	elif [ "$response" -eq 1 ]; then
		echo "Manual values for installation ..."
		username_network=$(dialog --title "Postgres DB Network Server !" --inputbox "username:" 10 70 3>&1 1>&2 2>&3 3>&-)
		password_network=$(dialog --title "Postgres DB Network Server !" --inputbox "password:" 10 70 3>&1 1>&2 2>&3 3>&-)
	
		username_app=$(dialog --title "Postgres DB Application Server !" --inputbox "username:" 10 70 3>&1 1>&2 2>&3 3>&-)
		password_app=$(dialog --title "Postgres DB Application Server !" --inputbox "password:" 10 70 3>&1 1>&2 2>&3 3>&-)
	else
		echo "Abort Installation !"
		exit 1
	fi

}

input_hardware_config(){
  hardware_driver=$(dialog --radiolist "Hardware Drivers / LoraWan Concentrators" 10 40 2 \
    1 "Dragino" off \
    2 "ChripStack Concentrator" on \
    3>&1 1>&2 2>&3 3>&-
  )
}


input_setup_config(){
	addtional_software=$(dialog --checklist "Setup Settings" 10 40 3 \
		1 "Dashboard with Grafana" on \
		2 "Alarm System" off \
		3 "Custom Website" off \
    3>&1 1>&2 2>&3 3>&-
  )
}

# greeting dialog and checks if dialog is installed and there is a working internet connection
is_dialog_installed || error "no dialog or vaulty package manager"
welcomedialog || error "user aborted"
abort_duo_connection || error "no connection"

# which gateways software do you wanna use 
input_hardware_config || error "user aborted"
# database auth 
input_postgres_settings || error "user aborted or no valid inputbox"


# Main Installation 
debian_install $hardware_driver || error "failure during installation"


input_setup_config || error "user aborted"


if [ "$addtional_software" -eq 1 ]; then
  debian_install_influx || error "unable to install influxdb"
  debian_install_grafana || error "unable to install grafana"
else 
  echo "no additional software: $addtional_software"
fi


