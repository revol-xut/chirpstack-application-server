#!/bin/sh
# ChirpStack Setup Script for Raspberry Pis
# by Tassilo Tanneberger <revol-xut@protonmail.com>
# License: GPLv2


# Include functions for Installation
# Scripts for installing postgres and creating the 
. ./postgresql/install_postgresql.sh
. ./postgresql/database_setup.sh

# Dragino
. ./dragino/dragino_install.sh
. ./dragino/dragino_install.sh

# Mosquitto Mqtt broker
. ./mosquitto/install_mosquitto.sh

# Redis
. ./redis/install_redis.sh

# Grafana 
. ./grafana/install.sh

add_chirpstack_repo(){\
	apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 1CE2AFD36DBCCA00
	echo "deb https://artifacts.chirpstack.io/packages/3.x/deb stable main" | tee /etc/apt/sources.list.d/chirpstack.list
	apt update
}

install_gateway(){\
	apt install chirpstack-gateway-bridge
	# see config at /etc/chirpstack-gateway-bridge/chirpstack-gateway-bridge.toml
	
	#TODO: Detect if systemd is installed alternativly use init.d
	systemctl enable chirpstack-gateway-bridge
	systemctl start chirpstack-gateway-bridge
}

install_network_server(){\
	apt install chirpstack-network-server
	# see config at  /etc/chirpstack-network-server/chirpstack-network-server.toml
	#TODO: configure maybe write Python script for that
	# postgresql.dsn, postgresql.automigrate, network_server.net_id, network_server.band.name, metrics.timezone


	#TODO: init.d
	systemctl enable chirpstack-network-server
	systemctl start chirpstack-network-server

}

install_application_server(){\
	apt-get install chirpstack-application-server
	
	#TODO: init.d
	systemctl enable chirpstack-application-server
	systemctl start chirpstack-application-server
}


debian_install(){\
	# $1 are hardware drivers
  # Following Options are possible:
  # - 1: Dragino
  # - 2: ChirpStack Concentrator

  echo "Starting Installation for Debian System"
  
  # Adding ChirpStack Software to package list
  add_chirpstack_repo || error "unable to add chriptstack repo to your package manager" 
  
  #Detect Hardware and install dragino
  if [ "$1" -eq 1 ]; then
    download_dragino_package || error "unable to install dragino"
  elif [ "$2" -eq 2]; then
    echo "Not implemented"
    #TODO: add Concentrator install
  else
    echo "Invalid Input"
    #exit 1;
  fi

  debian_install_postgres || error "unable to install and setup postgresql"
  setup_postgres_db "chirpstack_ns" "chirpstack_ns" "chirpstack_ns" || error "unable to setup the postgres database"
  setup_postgres_db "chirpstack_as" "chirpstack_as" "chirpstack_as" || error "unable to setup the postgres database"
  install_extensions || error "unable to install postgress table extension"	

  debian_install_mosquitto || error "unable to install mosquitto"
  debian_install_redis || error "unable to install redis"

  # Installing ChirpStack Gateway 
  install_gateway || error "unable to install the chirptstack gateway"
  install_network_server || error "unable to install chirptstack-network-server"
  install_application_server || error "unable to install chirpstack-application-server"
}

