#!/bin/sh
# ChirpStack Setup Script for Raspberry Pis
# by Tassilo Tanneberger <revol-xut@protonmail.com>
# License: GPLv2

 if [ -f /etc/os-release ]; then
  . /etc/os-release # imports variables
  OS=$NAME
  VERSION=$VERSION_ID
  DISTRO=$ID_LIKE
 elif type lsb_release > /dev/null 2>&1; then
  OS=$(lsb_release -sd)
  VERSION=$(lsb_release -sr)
  DISTRO=$(lsb_release -si)
elif [ -f /etc/lsb-release ]; then
  . /etc/lsb-release # imports variables
  OS=$DISTRIB_DESCRIPTION
  VERSION=$DISTRIB_RELEASE
  DISTRO=$DISTRIB_ID
elif [ -f /etc/debian_version ]; then
  # for older debians
  OS="Debian"
  VERSION=$(cat /etc/debian_version)
  DISTRO="debian"
elif [ -f /etc/SuSe-release ]; then
  # suse systems
  OS="Suse Linux"
  VERSION="";
  DISTRO="debian"
elif [-f /etc/redhat-release ]; then
  # redhats or cent os
  OS="RedHat Linux"
  VERSION=""
  DISTRO="debian"
else
  OS=$(uname -s)
  VERSION=$(uname -r)
fi
	
echo "Found Operating System: $OS with version $VERSION and base system: $DISTRO"

