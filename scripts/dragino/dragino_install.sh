#!/bin/sh
# ChirpStack Setup Script for Raspberry Pis
# by Tassilo Tanneberger <revol-xut@protonmail.com>
# License: GPLv2


download_dragino_package(){
	# Downlaods newest package
	curl http://www.dragino.com/downloads/downloads/LoRa_Gateway/PG1301/software/lorapktfwd.deb --output lorapktfwd.deb
	
	# Installs package
	dpkg -i lorapktfwd.deb
  
  # remove file
  rm lorapktfwd.deb
}


configure_dragino_gateway(){
	python3 generate_config.py --region "EU868"  	
}


enable_dragino(){
	systemctl enable lorapktfwd.service
	systemctl start lorapktfwd.service
}



