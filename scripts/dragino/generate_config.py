#!/bin/sh
# ChirpStack Setup Script for Raspberry Pis
# by Tassilo Tanneberger <revol-xut@protonmail.com>
# License: GPLv2

import uuid
import json

# See scripts/lookup/frequencies.json TODO:
valid_frequency_specifier: list = ["EU868", "AS923", "CN779", "AU915", "EU433", "KR920", "CN470", "US915"]


def generate_lora_gateway_id() -> str:
    # getting the mac-address with the getnode method
    mac_address: str = str(hex(uuid.getnode()))[2:]

    # manipulating mac address to get the gateway id
    # by taking the first 4 byte 
    return mac_address + "ffff"


def update_local_conf(path: str) -> None:
    with open(path, "r+") as config_file:
        content: str = config_file.read()
        config: dict = json.loads(content)
        config["gateway_conf"]["gateway_ID"] = generate_lora_gateway_id()
        config_file.truncate(0)
        config_file.write(json.dumps(config, indent=4))

if __name__ == "__main__":
    print(generate_lora_gateway_id())
    update_local_conf("/etc/lora-gateway/global_conf.json")
