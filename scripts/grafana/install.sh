#!/bin/sh
# ChirpStack Setup Script for Raspberry Pis
# by Tassilo Tanneberger <revol-xut@protonmail.com>
# License: GPLv2

debian_install_grafana(){
  apt-get install -y adduser libfontconfig1
  wget https://dl.grafana.com/oss/release/grafana_7.3.6_armhf.deb
  dpkg -i grafana_7.3.6_armhf.deb 
}

