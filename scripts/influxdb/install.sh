#!/bin/sh
# ChirpStack Setup Script for Raspberry Pis
# by Tassilo Tanneberger <revol-xut@protonmail.com>
# License: GPLv2

debian_install_influxdb(){
  apt update
  apt upgrade

  wget -qO- https://repos.influxdata.com/influxdb.key | sudo apt-key add -
  echo "deb https://repos.influxdata.com/debian buster stable" | sudo tee /etc/apt/sources.list.d/influxdb.influxdb.list
  
  apt update
  apt install influxdb

  systemctl unmask influxdb
  systemctl enable influxdb
  systemctl start influxdb
}

