#!/bin/sh
# ChirpStack Setup Script for Raspberry Pis
# by Tassilo Tanneberger <revol-xut@protonmail.com>
# License: GPLv2


debian_install_mosquitto(){
	apt-get update

	apt-get install -y mosquitto mosquitto-clients

	systemctl enable mosquitto.service
	systemctl start mosquitto.service
}

