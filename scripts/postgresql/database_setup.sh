#!/bin/sh
# ChirpStack Setup Script for Raspberry Pis
# by Tassilo Tanneberger <revol-xut@protonmail.com>
# License: GPLv2


setup_postgres_db(){
  #$1 = username $2 = user_password $3 = database_name
  sudo -u postgres psql -c "CREATE ROLE $1 WITH LOGIN PASSWORD '$2';"
  sudo -u postgres psql -c "CREATE DATABASE chripstack_ns WITH OWNER $1;"
  
}

installl_extensions(){
  # Installing Extensions for chirpstack_ns und chirpstack_as 
  sudo -u postgres psql -c "USE chirpstack_ns; CREATE EXTENSION pg_trgm;"
  sudo -u postgres psql -c "USE chirpstack_ns; CREATE EXTENSION hstore;"

  sudo -u postgres psql -c "USE chirpstack_as; CREATE EXTENSION pg_trgm;"
  sudo -u postgres psql -c "USE chirpstack_as; CREATE EXTENSION hstore;"
}


verify_creation(){
  #$1 = database_name
  sudo -u postgres psql -c "\l" | grep -e "$1"  
  
}

