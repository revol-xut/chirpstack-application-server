#!/usr/bin/python3
# ChirpStack Setup Script for Raspberry Pis
# by Tassilo Tanneberger <revol-xut@protonmail.com>
# License: GPLv2

import toml
import sys

network_server_config: str = "/etc/chirpstack-network-server/chirpstack-network-server.toml"
application_server_config: str = "/etc/chirpstack-network-server/chirpstack-network-server.toml"


def write_dsn(password: str, username: str, path: str):
    dsn: str = "postgres://" + password + ":" + username + "@localhost/chirpstack_ns?sslmode=disable"

    with open(path, "w") as config_file:
        toml_obj = toml.loads(config_file)
        toml_obj["dsn"] = dsn;
        toml.dump(toml_obj, config_file)


if __name__ == "__main__":
    write_dsn(sys.argv[1], sys.argv[2], network_server_config)
    write_dsn(sys.argv[3], sys.argv[4], application_server_config)
