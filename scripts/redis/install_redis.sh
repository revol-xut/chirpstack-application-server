#!/bin/sh
# ChirpStack Setup Script for Raspberry Pis
# by Tassilo Tanneberger <revol-xut@protonmail.com>
# License: GPLv2

debian_install_redis(){
	apt-get update

	apt-get install redis-server
}

